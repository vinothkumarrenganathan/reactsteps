/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import FadeInView from './FadeInView';
import {Platform, StyleSheet, Animated, Text, View, SectionList, Alert, TextInput} from 'react-native';
import FontAwesome, { Icons } from "react-native-fontawesome";
import ReactSteps from './ReactSteps';
import { Step } from './ReactSteps';

export default class App extends React.Component {

  state = {
    currentPositions: [0, 2]
  }

  componentWillMount() {
    Step(this.props)
  }

  handleChange = (value) => {
    this.setState({currentPositions: value})
  }

  render() {
    return (
      <ReactSteps currentPositions={this.state.currentPositions} onHandleChange={this.handleChange}>
        <Step title="Beginner">
          <Text>Hello</Text>
          <Text>Hello1</Text>
          <Text>Hello2</Text>
        </Step>
        <Step title="Intermediate">
          <Text>World</Text>
          <TextInput style={{height: 40}} placeholder="Something Type here"/>
        </Step>
        <Step title="Advanced">
          <Text>By Vinoth</Text>
        </Step>
      </ReactSteps>
    )
  };
    
}

